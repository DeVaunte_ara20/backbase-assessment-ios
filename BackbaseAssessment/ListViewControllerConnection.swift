//
//  ListViewControllerConnection.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import Foundation
extension ViewController : NSURLSessionDelegate, NSURLSessionDataDelegate {
    
    func httpGet(request: NSURLRequest!) -> Void {
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request){ (data, response, error) -> Void in
            if error != nil {
                print(error);
            } else {
                let json = data;
                self.myData = NSMutableData(data: json!);
                
                let queue = dispatch_queue_create("Backbase.BackbaseAssessment", DISPATCH_QUEUE_CONCURRENT)
                dispatch_sync(queue, {
                    
                    if self.myData.length != 0 {
                        self.pullingEmployeeInformation()
                        self.myData.length = 0;
                        
                    }
                    
                    
                    
                })
            }
        }
        task.resume()
    }
    
    
}
