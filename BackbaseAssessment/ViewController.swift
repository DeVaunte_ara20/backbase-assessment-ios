//
//  ViewController.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var launchpadArray : [Employee] = []
    var cxpArray : [Employee] = []
    var mobileArray : [Employee] = []
    var sections = ["LaunchPad","CXP","Mobile"]
    
    
    @IBOutlet weak var EmployeeTableView: UITableView!
    
    var myData = NSMutableData()
    var error : NSError!
    var request : NSMutableURLRequest!
    
    var pullURL : NSURL?
    var url : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("networkStatusChanged:"), name: ReachabilityStatusChangedNotification, object: nil)
        Reach().monitorReachabilityChanges()
        let status = Reach().connectionStatus()
        switch status {
        case .Unknown, .Offline:
            print("Not connected")
            
            print("Internet connection FAILED")
            let Alert = UIAlertController(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", preferredStyle: UIAlertControllerStyle.Alert)
            Alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            let Settings = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction) in
                UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
                           })
            
            Alert.addAction(Settings)
            

            
            
            
            self.presentViewController(Alert, animated: true, completion: nil)
            
            
        case .Online(.WWAN):
            print("Internet connection OK")

            print("Connected via WWAN")
            url  = "http://nielsmouthaan.nl/backbase/members.php"
            let myUrl = NSURL(string: "\(url)");
            if let url = myUrl {
                request = NSMutableURLRequest(URL: url)
                httpGet(request);
            }

        case .Online(.WiFi):
            print("Internet connection OK")

            print("Connected via WiFi")
            url  = "http://nielsmouthaan.nl/backbase/members.php"
            let myUrl = NSURL(string: "\(url)");
            if let url = myUrl {
                request = NSMutableURLRequest(URL: url)
                httpGet(request);
            }

        }
        
        
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func networkStatusChanged(notification: NSNotification) {
        let userInfo = notification.userInfo
        print(userInfo)
    }
    override func viewWillAppear(animated: Bool) {
        EmployeeTableView?.registerNib(UINib(nibName: "BackbaseEmployeeTableViewCell", bundle: nil), forCellReuseIdentifier: "reuseIdentifier")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

