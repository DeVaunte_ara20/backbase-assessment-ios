//
//  BackbaseEmployeeTableViewCell.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import UIKit

class BackbaseEmployeeTableViewCell: UITableViewCell {
    var myImage = NSMutableData()
    
    var request : NSMutableURLRequest!
    
    
    
    @IBOutlet weak var employeeImage: UIImageView!
    @IBOutlet weak var employeeName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setAlbumImage(artworkString : String, delegateVC : UIViewController) {
        
        let url = NSURL(string: artworkString)
        if let myUrl = url {
            request = NSMutableURLRequest(URL: myUrl)
            httpGet(request);
        }
        
    }
}
extension BackbaseEmployeeTableViewCell : NSURLSessionDelegate, NSURLSessionDataDelegate {
    
    func httpGet(request: NSURLRequest!) -> Void {
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request){ (data, response, error) -> Void in
            
            var downloadedImage : UIImage? = nil;
            
            if data != nil {
                downloadedImage = UIImage(data: data!)
            }
            
            if downloadedImage == nil {
                downloadedImage = UIImage(named: "Default");
            }
            
            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                self.employeeImage.image = downloadedImage;
                self.myImage.length = 0
                
            })
            
        }
        task.resume()
    }
    
    
}



