//
//  DetailViewController.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import UIKit
import MessageUI

class DetailViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var RoleLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var employeeImage: UIImageView!
    var fullName :String = ""
    var email: String = ""
    var imageOfEmployee:String = ""
    var role:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nameLabel.text = fullName
        RoleLabel.text = "Role: " + role
        emailLabel.text = "Email: " + email
        
        if imageOfEmployee == "Default" || imageOfEmployee == "http://nielsmouthaan.nl/backbase/photos/JoseCortes.jpg" {
            employeeImage.image = UIImage(named: "noimage.jpg")
            
        }else{
            let url = NSURL(string: imageOfEmployee)
            
            let imageData = NSData(contentsOfURL: url!)
            let image = UIImage(data: imageData!)
            employeeImage.image = image
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("imageTapped:"))
            employeeImage.userInteractionEnabled = true
            employeeImage.addGestureRecognizer(tapGestureRecognizer)
        }
        
        
        
    }
    @IBAction func launchEmail(sender: AnyObject) {
        
        let emailTitle = "Backbase Assessment"
        let messageBody = ""
        let toRecipents = [email]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        self.presentViewController(mc, animated: true, completion: nil)
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Mail saved")
        case MFMailComposeResultSent.rawValue:
            print("Mail sent")
        case MFMailComposeResultFailed.rawValue:
            print("Mail sent failure: %@", [error!.localizedDescription])
        default:
            break
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imageTapped(img: AnyObject)
    {
        let url = NSURL(string: imageOfEmployee)
        let imageData = NSData(contentsOfURL: url!)
        let image = UIImage(data: imageData!)
        
        let newImageView = UIImageView(image: image)
        newImageView.frame = self.view.frame
        newImageView.backgroundColor = .blackColor()
        newImageView.contentMode = .ScaleAspectFit
        newImageView.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: "dismissFullscreenImage:")
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
