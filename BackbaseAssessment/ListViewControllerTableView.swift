//
//  ListViewControllerTableView.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return sections.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section == 0{
            
            return launchpadArray.count
            
        }else if section == 1{
            return cxpArray.count
            
        }else{
            return mobileArray.count
            
        }
    }
    
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView //recast your view as a UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.whiteColor() //make the background color light blue
        header.textLabel!.textColor = UIColor.blackColor() //make the text white
        header.alpha = 0.5 //make the header transparent
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if (section == 0){
            return sections[0]
            
        }
        if (section == 1){
            
            return sections[1]
        }
        if (section == 2){
            return sections[2]
        }
        
        
        
        return nil
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = EmployeeTableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! BackbaseEmployeeTableViewCell
        
        
        //Configure the cell...
        if (indexPath.section == 0){
            cell.employeeName?.text = launchpadArray[indexPath.row].firstName + " " + launchpadArray[indexPath.row].lastname
            if launchpadArray[indexPath.row].photo == "Default"{
                cell.employeeImage.image = UIImage(named: "noimage.jpg")
            }else{
                cell.setAlbumImage(launchpadArray[indexPath.row].photo, delegateVC: self)
                
            }
            
        }
        else if (indexPath.section == 1)
        {
            cell.employeeName?.text = cxpArray[indexPath.row].firstName + " " + cxpArray[indexPath.row].lastname
            if cxpArray[indexPath.row].photo == "Default"{
                cell.employeeImage.image = UIImage(named: "noimage.jpg")
            }else{
                cell.setAlbumImage(cxpArray[indexPath.row].photo, delegateVC: self)
                
            }
            
        }
        else if (indexPath.section == 2)
        {
            cell.employeeName?.text = mobileArray[indexPath.row].firstName + " " + mobileArray[indexPath.row].lastname
            if mobileArray[indexPath.row].photo == "Default" || mobileArray[indexPath.row].photo == "http://nielsmouthaan.nl/backbase/photos/JoseCortes.jpg"{
                cell.employeeImage.image = UIImage(named: "noimage.jpg")
            }else{
                cell.setAlbumImage(mobileArray[indexPath.row].photo, delegateVC: self)
                
            }
            
            
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        performSegueWithIdentifier("toDetails", sender: indexPath)
        return nil
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let detailVC : DetailViewController = segue.destinationViewController as! DetailViewController
        let indexPath:NSIndexPath = sender as! NSIndexPath
        
        if indexPath.section == 0{
            
            detailVC.fullName = launchpadArray[indexPath.row].firstName + " " + launchpadArray[indexPath.row].lastname
            
            detailVC.email = launchpadArray[indexPath.row].email
            detailVC.imageOfEmployee = launchpadArray[indexPath.row].photo
            detailVC.role = launchpadArray[indexPath.row].role
            
            
        }else if indexPath.section == 1{
            
            detailVC.fullName = cxpArray[indexPath.row].firstName + " " + cxpArray[indexPath.row].lastname
            
            detailVC.email = cxpArray[indexPath.row].email
            detailVC.imageOfEmployee = cxpArray[indexPath.row].photo
            detailVC.role = cxpArray[indexPath.row].role
            
        }
        else if indexPath.section == 2{
            
            detailVC.fullName = mobileArray[indexPath.row].firstName + " " + mobileArray[indexPath.row].lastname
            
            detailVC.email = mobileArray[indexPath.row].email
            detailVC.imageOfEmployee = mobileArray[indexPath.row].photo
            detailVC.role = mobileArray[indexPath.row].role
        }
        
        
        
        
    }
    
}
