//
//  backBaseEmployeeCustomCLass.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import Foundation

class Employee {
    
    
    var firstName : String
    var lastname : String
    var email : String
    var photo : String
    var role : String
    
    
    
    init() {
        self.firstName = "firsName"
        self.lastname = "lastname"
        self.email = "email"
        self.photo = "photo"
        self.role = "role"
        
        
    }
    init(firstName : String, lastname : String, email : String, photo : String, role: String) {
        self.firstName = firstName
        self.lastname = lastname
        self.email = email
        self.photo = photo
        self.role = role
    }
}