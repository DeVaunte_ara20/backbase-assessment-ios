//
//  ListViewControllerFunctions.swift
//  BackbaseAssessment
//
//  Created by DeVaunte Ledee on 3/13/16.
//  Copyright © 2016 DeVaunte Ledee. All rights reserved.
//

import Foundation
import UIKit
extension ViewController{
    
    
    func pullingEmployeeInformation(){
        print("PARSING WITH SWITFYJSON")
        pullLaunchpadEmployees()
        pullCXPEmployees()
        pullMobileEmployees()
        dispatch_async(dispatch_get_main_queue()) {
            self.EmployeeTableView.reloadData()
        }
    }
    
    func pullLaunchpadEmployees(){
        
        let response = JSON(data: myData);
        if let children = response["Launchpad"].array {
            var photo = ""
            for child in children {
                
                let firstName = child["name"].string!
                let lastName = child["surname"].string!
                let email = child["email"].string!
                let role = child["role"].string!
                
                print(firstName)
                print(lastName)
                print(email)
                print(role)
                
                if child["photo"] == nil{
                    photo = "Default"
                } else {
                    photo = "http://nielsmouthaan.nl/backbase/photos/\(child["photo"].string!)"
                    print(photo)
                    
                }
                
                let launchpadEmployee = Employee(firstName: firstName, lastname: lastName, email: email, photo: photo, role: role)
                
                launchpadArray.append(launchpadEmployee)
                //                EmployeeTableView.reloadData()
                
                
            }
        }
    }
    func pullCXPEmployees(){
        
        let response = JSON(data: myData);
        if let children = response["CXP"].array {
            var photo = ""
            for child in children {
                
                let firstName = child["name"].string!
                let lastName = child["surname"].string!
                let email = child["email"].string!
                let role = child["role"].string!
                
                print(firstName)
                print(lastName)
                print(email)
                print(role)
                
                if child["photo"] == nil{
                    photo = "Default"
                } else {
                    photo = "http://nielsmouthaan.nl/backbase/photos/\(child["photo"].string!)"
                    print(photo)
                    
                }
                
                let cxpEmployee = Employee(firstName: firstName, lastname: lastName, email: email, photo: photo, role: role)
                
                cxpArray.append(cxpEmployee)
                
            }
        }
    }
    func pullMobileEmployees(){
        
        let response = JSON(data: myData);
        if let children = response["Mobile"].array {
            var photo = ""
            for child in children {
                
                let firstName = child["name"].string!
                let lastName = child["surname"].string!
                let email = child["email"].string!
                let role = child["role"].string!
                
                print(firstName)
                print(lastName)
                print(email)
                print(role)
                
                if child["photo"] == nil || child[""] == "JoseCortes.jpg"{
                    photo = "Default"
                } else {
                    photo = "http://nielsmouthaan.nl/backbase/photos/\(child["photo"].string!)"
                    print(photo)
                    
                }
                
                let mobileEmployee = Employee(firstName: firstName, lastname: lastName, email: email, photo: photo, role: role)
                
                mobileArray.append(mobileEmployee)
                
                
            }
        }
    }
    
    
    
}
